# Password Generator

Password Generator est un projet de générateur de mot de passe développé en utilisant les technologies HTML, CSS et JavaScript. Il permet à l'utilisateur de générer des mots de passe sécurisés de manière aléatoire en spécifiant la longueur du mot de passe et les types de caractères à inclure (lettres, chiffres, symboles).Facile à utiliser, ce qui en fait un outil pratique pour tous ceux qui souhaitent créer des mots de passe forts et sécurisés.
